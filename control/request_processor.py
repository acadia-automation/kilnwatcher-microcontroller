# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class RequestProcessor:
    REQUEST_OP_DATA_SEPARATOR = ':'
    TEXT_ENCODING = 'utf-8'
    RESPONSE_FAILURE_INVALID_REQUEST = 'Invalid Request:'

    def __init__(self, serial, request_registry):
        self.__serial = serial
        self.__request_registry = request_registry

    def process_requests(self):
        if self.check_for_requests():
            request = self.read_next_request()
            request_parsed = request.split(RequestProcessor.REQUEST_OP_DATA_SEPARATOR)

            if self.validate_request(request_parsed[0]):
                action = self.map_request_to_action(request_parsed[0])
                if len(request_parsed) > 1:
                    action.perform_action(data=request_parsed[1])
                else:
                    action.perform_action()
                self.respond_to_request(action.get_action_response())
            else:
                # Invalid request, respond with a failure
                self.respond_to_request(RequestProcessor.RESPONSE_FAILURE_INVALID_REQUEST + request)

    def check_for_requests(self):
        if self.__serial.in_waiting > 0:
            return True

    def read_next_request(self) -> str:
        bytes_read = self.__serial.readline(50)
        return bytes_read.decode(RequestProcessor.TEXT_ENCODING).strip()

    def validate_request(self, request):
        return self.__request_registry.is_valid_request(request)

    def map_request_to_action(self, request):
        return self.__request_registry.get_action_for_request(request)

    def respond_to_request(self, response):
        encoded_response = response.encode(RequestProcessor.TEXT_ENCODING)
        return self.__serial.write(encoded_response)
