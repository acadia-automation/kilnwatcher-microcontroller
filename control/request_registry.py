# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class RequestRegistry:

    def __init__(self):
        self.__registry = {}

    def register_request_type(self, request_code, action):
        self.__registry[request_code] = action

    def is_valid_request(self, request_code):
        return request_code in self.__registry

    def get_action_for_request(self, request_code):
        return self.__registry[request_code]
