# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from actions.action import Action
from actions.action_indicator_kiln_temp_range import ActionIndicatorKilnTempRange
from actions.action_indicator_off import ActionIndicatorOff
from actions.action_indicator_on import ActionIndicatorOn
from actions.action_indicator_test import ActionIndicatorTest
from actions.action_indicator_warn import ActionIndicatorWarn
from actions.action_temperature_read_primary import ActionTemperatureReadPrimary
from actions.action_temperature_read_secondary import ActionTemperatureReadSecondary
from control.request_registry import RequestRegistry
from indicators.indicator_alchemist import IndicatorAlchemist


class ActionRegistration:

    @staticmethod
    def register_available_actions(kiln_indicator, case_indicator, fan_indicator, ambient_temp_sensor, case_temp_sensor,
                                   kiln_temp_sensor) -> RequestRegistry:
        registry = RequestRegistry()

        # Indicator actions
        registry.register_request_type(Action.INDICATOR_ON_KILN,
                                       ActionIndicatorOn(kiln_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_OFF_KILN,
                                       ActionIndicatorOff(kiln_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_RANGE_KILN,
                                       ActionIndicatorKilnTempRange(kiln_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_WARN_KILN,
                                       ActionIndicatorWarn(kiln_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_TEST_KILN,
                                       ActionIndicatorTest(kiln_indicator, IndicatorAlchemist.get_alchemist()))

        registry.register_request_type(Action.INDICATOR_ON_CASE,
                                       ActionIndicatorOn(case_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_OFF_CASE,
                                       ActionIndicatorOff(case_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_WARN_CASE,
                                       ActionIndicatorWarn(case_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_TEST_CASE,
                                       ActionIndicatorTest(case_indicator, IndicatorAlchemist.get_alchemist()))

        registry.register_request_type(Action.INDICATOR_ON_FAN,
                                       ActionIndicatorOn(fan_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_OFF_FAN,
                                       ActionIndicatorOff(fan_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_TEST_FAN,
                                       ActionIndicatorTest(fan_indicator, IndicatorAlchemist.get_alchemist()))
        registry.register_request_type(Action.INDICATOR_WARN_FAN,
                                       ActionIndicatorWarn(fan_indicator, IndicatorAlchemist.get_alchemist()))

        # Sensor Actions
        registry.register_request_type(Action.AMBIENT_READ_TEMP, ActionTemperatureReadPrimary(ambient_temp_sensor))
        registry.register_request_type(Action.AMBIENT_READ_HUMIDITY,
                                       ActionTemperatureReadSecondary(ambient_temp_sensor))

        registry.register_request_type(Action.CASE_READ_TEMP, ActionTemperatureReadPrimary(case_temp_sensor))
        registry.register_request_type(Action.CASE_READ_HOUSING_TEMP, ActionTemperatureReadSecondary(case_temp_sensor))

        registry.register_request_type(Action.KILN_READ_TEMP, ActionTemperatureReadPrimary(kiln_temp_sensor))
        registry.register_request_type(Action.KILN_READ_AMP_TEMP, ActionTemperatureReadSecondary(kiln_temp_sensor))

        return registry
