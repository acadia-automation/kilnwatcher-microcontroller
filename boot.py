# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import usb_cdc

usb_cdc.enable(console=True, data=True)
