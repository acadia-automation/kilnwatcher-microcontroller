# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from random import random, randint

from indicators.indicator import Indicator
from indicators.color import Color
from indicators.transmutation import Transmutation


class TransmutationFlicker(Transmutation):

    def __init__(self, indicator: Indicator, update_interval: int, flicker_factor: int, flicker_range: int):
        super().__init__(indicator, update_interval)
        self.__currently_flickering: bool = False
        self.flicker_factor: int = flicker_factor
        self.flicker_range: int = flicker_range
        self.__original_interval_sec: float = self.update_interval_sec

    def step(self, now: float) -> bool:
        if self._time_for_update(now):
            if self.__currently_flickering:
                self.__reset_to_steady()
            elif self.__flicker_this_round():
                self.__start_flickering()
        return True

    def __reset_to_steady(self):
        # Debug: print("Resetting to steady")
        self.target_indicator.set_all(self.target_indicator.target_color())
        self.update_interval_sec = self.__original_interval_sec
        self.__currently_flickering = False

    def __flicker_this_round(self):
        return randint(1, 10) <= self.flicker_factor

    def __start_flickering(self):
        # Debug: print("Flickering")
        self.update_interval_sec = self.__pick_flicker_interval(self.__original_interval_sec)
        self.target_indicator.set_pixel(
            self.__pick_flicker_pixel(self.target_indicator.PIXELS_PER_BOARD),
            self.__pick_flicker_color(self.target_indicator.target_color()))
        self.__currently_flickering = True

    def __pick_flicker_color(self, target_color: Color):
        new_color = list(target_color.value)
        flicker_amount = randint(1, self.flicker_range)
        flicker_hue = randint(0, 3)
        if new_color[flicker_hue] > flicker_amount:
            new_color[flicker_hue] -= flicker_amount
        else:
            new_color[flicker_hue] = 0
        return Color(*new_color)

    @staticmethod
    def __pick_flicker_interval(base_interval: float):
        return random() * base_interval * 2

    @staticmethod
    def __pick_flicker_pixel(number_of_pixels):
        return randint(1, number_of_pixels)
