# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from indicators.indicator import Indicator
from indicators.color import Color
from indicators.transmutation import Transmutation


class TransmutationFade(Transmutation):
    MODE_SINGLE = 1
    """ Fade between start and end, leaving the indicator at the end color """

    MODE_REPEAT = 2
    """ Fade between start and end, then repeat indefinitely """

    MODE_BOUNCE = 3
    """ Fade between start and end, then back to start, leaving the indicator at the start color """

    MODE_BOUNCE_REPEAT = 4
    """ Fade between start and end, then back to start, repeat indefinitely """

    DIR_FORWARD = 1
    DIR_REVERSE = 2

    def __init__(self, indicator: Indicator, update_interval: int, start: Color, end: Color, steps: int,
                 mode: int = MODE_SINGLE):
        super().__init__(indicator, update_interval)
        self.__start: Color = start
        self.__end: Color = end
        self.__steps: int = steps
        self.__mode: int = mode

        self.__cur_step: int = 0
        self.__direction = TransmutationFade.DIR_FORWARD
        self.__complete = False

    def step(self, now: float) -> bool:
        if self._time_for_update(now):
            if self.__transition():
                # Update the current step taking direction into account
                if self.__direction == TransmutationFade.DIR_FORWARD:
                    self.__cur_step += 1
                else:
                    self.__cur_step -= 1

                # Check for boundary condition and update state as necessary
                self.__check_boundaries()
            else:
                return False
        return True

    def is_complete(self) -> bool:
        return self.__complete

    def finalize(self) -> bool:
        if self.__mode == TransmutationFade.MODE_SINGLE:
            self.target_indicator.set_all(self.__end)
        elif self.__mode == TransmutationFade.MODE_BOUNCE:
            self.target_indicator.set_all(self.__start)
        return True

    def __transition(self):
        next_color: list[int] = []
        for i in range(0, 4):
            next_color.append(int(((self.__start.value[i] * (self.__steps - self.__cur_step)) + (
                    self.__end.value[i] * self.__cur_step)) / self.__steps))
        self.target_indicator.set_all(Color(*next_color))
        return True

    def __check_boundaries(self):
        """
        Handles when the transition reaches an upper or lower boundary and responds accordingly
        """
        if self.__cur_step >= self.__steps and self.__direction == TransmutationFade.DIR_FORWARD:
            if self.__mode == TransmutationFade.MODE_REPEAT:
                self.__cur_step = 0
            elif self.__mode == TransmutationFade.MODE_BOUNCE or self.__mode == TransmutationFade.MODE_BOUNCE_REPEAT:
                self.__reverse()
            else:
                self.__complete = True

        elif self.__cur_step <= 0 and self.__direction == TransmutationFade.DIR_REVERSE:
            if self.__mode == TransmutationFade.MODE_BOUNCE:
                self.__complete = True
            elif self.__mode == TransmutationFade.MODE_BOUNCE_REPEAT:
                self.__reverse()

    def __reverse(self):
        if self.__direction == TransmutationFade.DIR_FORWARD:
            self.__direction = TransmutationFade.DIR_REVERSE
            self.__cur_step = self.__steps - 1
        else:
            self.__direction = TransmutationFade.DIR_FORWARD
            self.__cur_step = 0
