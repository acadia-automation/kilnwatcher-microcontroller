# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from neopixel import NeoPixel

from indicators.color import Color
from indicators.indicator import Indicator


class IndicatorImpl(Indicator):
    """
    Represents an Adafruit NeoPixel jewel board that is treated like a
    singular indicator light.

    Note, this could easily be generalised to represent any number of NeoPixel
    form factors.
    """
    # NeoPixel Jewel boards are used for the indicators and they have 7 pixels each

    # Useful colors

    def __init__(self, sequence: int, pixels: NeoPixel, target_color: Color = None):
        self.__target_color: Color = target_color

        self.__sequence: int = sequence
        self.__pixels: NeoPixel = pixels

    def set_all(self, color: Color):
        """
        Changes the color value of the IndicatorImpl to the provided color

        :param color:
        Color
            new color for the pixel
        """
        # Debug: print(f"Pixel Range:{self.__pixel_range()}")
        for i in self.__pixel_range():
            self.__pixels[i] = color.value
            # Debug: print(f"\tPixel {i} :\t{color.value}")
        self.__pixels.show()

    def set_pixel(self, pixel_number: int, color: Color):
        """
        Changes the color value of a single pixel on this IndicatorImpl to the
        provided color

        :param pixel_number:
        int
            pixel number from 1 to IndicatorImpl.PIXELS_PER_BOARD
        :param color:
        Color
            new color for the pixel
        """
        self.__pixels[self.__absolute_index(pixel_number)] = color.value
        self.__pixels.show()

    def current_pixel_color(self, pixel_number: int) -> Color:
        """
        Identifies the current color displayed on the specified pixel on this
        IndicatorImpl.

        See IndicatorImpl.current_color for additional notes.
        :param pixel_number:
        int
            pixel number from 1 to IndicatorImpl.PIXELS_PER_BOARD
        :returns:
        Color
            current color set for the pixel
        """
        return Color(*self.__pixels[self.__absolute_index(pixel_number)])

    def current_color(self) -> Color:
        return self.current_pixel_color(1)

    def target_color(self) -> Color:
        return self.__target_color

    def __pixel_range(self) -> range:
        """
        Calculates the absolute range of pixel indices on the NeoPixel object
        that drives this IndicatorImpl.
        :return:
        """
        return range(self.__sequence * IndicatorImpl.PIXELS_PER_BOARD,
                     ((self.__sequence + 1) * IndicatorImpl.PIXELS_PER_BOARD))

    def __absolute_index(self, pixel_number: int) -> int:
        return self.__pixel_range()[0] + pixel_number - 1
