# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import time

from indicators.indicator import Indicator
from indicators.transmutation import Transmutation


class IndicatorAlchemist:
    __instance = None

    @classmethod
    def get_alchemist(cls):
        """
        This is the only way an instance of IndicatorAlchemist should be retrieved, do
        not use the constructor directly. This class behaves as a singleton and
        if an instance already exists, this method will return it.
        """
        if cls.__instance:
            return cls.__instance
        else:
            cls.__instance = cls()
            return cls.__instance

    def __init__(self):
        # Todo: Switch to deque if it becomes available on CircuitPython
        self.__transmutation_queues: dict[Indicator, list] = {}
        self.__current_transmutations: dict[Indicator, Transmutation] = {}

    def step(self) -> bool:
        success = True
        for tmu in list(self.__current_transmutations.values()):
            if tmu.is_complete():
                success = success and tmu.finalize() and self.__next_transmutation(tmu.target_indicator)
            else:
                success = success and tmu.step(time.monotonic())
        return success

    def queue_transmutation(self, tmu: Transmutation) -> int:
        if tmu.target_indicator in self.__transmutation_queues:
            self.__transmutation_queues[tmu.target_indicator].append(tmu)
        else:
            self.__transmutation_queues[tmu.target_indicator] = [tmu]
        # Debug: print(f"Transmutation Queue: {self.__transmutation_queues}")
        return len(self.__transmutation_queues[tmu.target_indicator])

    def transmute_immediately(self, tmu: Transmutation) -> bool:
        self.__transmutation_queues[tmu.target_indicator] = [tmu]
        # Debug: print(f"Transmutation Queue: {self.__transmutation_queues}")
        return self.__next_transmutation(tmu.target_indicator)

    def __next_transmutation(self, indicator: Indicator):
        if indicator in self.__current_transmutations:
            self.__current_transmutations.pop(indicator, None)
        if self.__transmutation_queues[indicator]:
            if len(self.__transmutation_queues[indicator]) > 0:
                self.__current_transmutations[indicator] = self.__transmutation_queues[indicator].pop(0)
        # Debug: print(f"Current Transmutations: {self.__current_transmutations}")
        # Debug: print(f"Transmutation Queue: {self.__transmutation_queues}")
        return True

    @property
    def transmutation_queues(self) -> dict[Indicator, list]:
        return self.__transmutation_queues

    @property
    def current_transmutations(self) -> dict[Indicator, Transmutation]:
        return self.__current_transmutations
