# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from indicators.color import Color
from indicators.indicator import Indicator


class IndicatorFake(Indicator):
    OFF = Color(0, 0, 0, 0)

    def __init__(self, sequence: int, target_color: Color = None):
        self.target_color: Color = target_color

        self.current_colors = [IndicatorFake.OFF, IndicatorFake.OFF, IndicatorFake.OFF, IndicatorFake.OFF, IndicatorFake.OFF, IndicatorFake.OFF, IndicatorFake.OFF]

    def set_all(self, color: Color):
        self.current_colors[0] = color

    def set_pixel(self, pixel_number: int, color: Color):
        self.current_colors[pixel_number - 1] = color

    def current_pixel_color(self, pixel_number: int) -> Color:
        return self.current_colors[pixel_number - 1]
