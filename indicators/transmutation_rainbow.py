# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import rainbowio

from indicators.color import Color
from indicators.indicator import Indicator
from indicators.transmutation import Transmutation


class TransmutationRainbow(Transmutation):

    def __init__(self, indicator: Indicator, update_interval: int, repeats: int):
        super().__init__(indicator, update_interval)
        self.__repeats: int = repeats

        self.__repeat_count = 0
        self.__current_wheel_index = 0

    def step(self, now: float) -> bool:
        if self._time_for_update(now):
            return self.__transition()
        return True

    def is_complete(self) -> bool:
        return self.__repeats != 0 and self.__repeats <= self.__repeat_count

    def finalize(self) -> bool:
        return True

    def __transition(self):
        if self.__current_wheel_index == 256:
            self.__current_wheel_index = 0
            self.__repeat_count += 1

        self.target_indicator.set_all(Color.rgbw_color_tuple_from_rgb(rainbowio.colorwheel(self.__current_wheel_index)))
        self.__current_wheel_index += 1
        return True
