# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from indicators.color import Color


class Indicator():
    COLOR_FAN_ON_BLUE = Color(0, 30, 150, 15)
    COLOR_CASE_HOT_RED = Color(150, 30, 0, 15)
    COLOR_KILN_HOT_GREEN = Color(30, 150, 0, 15)
    COLOR_OFF = Color(0, 0, 0, 0)
    PIXELS_PER_BOARD = 7

    def set_all(self, color):
        """
        Changes the color value of the IndicatorImpl to the provided color

        :param color:
        Color
            new color for the pixel
        """
        pass

    def set_pixel(self, pixel_number, color):
        """
        Changes the color value of a single pixel on this IndicatorImpl to the
        provided color

        :param pixel_number:
        int
            pixel number from 1 to IndicatorImpl.PIXELS_PER_BOARD
        :param color:
        Color
            new color for the pixel
        """
        pass

    def current_pixel_color(self, pixel_number):
        """
        Identifies the current color displayed on the specified pixel on this
        IndicatorImpl.

        See IndicatorImpl.current_color for additional notes.
        :param pixel_number:
        int
            pixel number from 1 to IndicatorImpl.PIXELS_PER_BOARD
        :returns:
        Color
            current color set for the pixel
        """
        pass

    def current_color(self) -> Color:
        """
        Identifies the current color set on the indicator. Note that this
        method uses a single pixel to represent the color of the whole
        IndicatorImpl. More detailed analysis can be performed by using the
        IndicatorImpl.current_pixel_color method and sampling multiple or all pixels.

        Additionally, this will report whatever color has been set, even if it
        is not currently being shown by the IndicatorImpl. (This is an unlikely
        condition to encounter, as this API groups setting and showing of the
        colors, synchronously.)
        :returns:
        Color
            current color set for the IndicatorImpl
        """
        pass

    def target_color(self) -> Color:
        pass
