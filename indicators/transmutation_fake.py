# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from indicators.indicator import Indicator
from indicators.transmutation import Transmutation


class TransmutationFake(Transmutation):

    def __init__(self, indicator: Indicator, update_interval: int, step_pass: bool = False,
                 is_complete_val: bool = False, finalize_pass: bool = False, time_for_update_val: bool = False):

        super().__init__(indicator, update_interval)
        self.step_pass = step_pass
        self.is_complete_val = is_complete_val
        self.finalize_pass = finalize_pass
        self.time_for_update_val = time_for_update_val

    def step(self, now: float) -> bool:
        return self.step_pass

    def is_complete(self) -> bool:
        return self.is_complete_val

    def finalize(self) -> bool:
        return self.finalize_pass

    def _time_for_update(self, now: float) -> bool:
        return self.time_for_update_val
