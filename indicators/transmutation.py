# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from indicators.indicator import Indicator


class Transmutation:

    def __init__(self, indicator: Indicator, update_interval: int):
        self.target_indicator = indicator
        self.update_interval_sec: float = update_interval / 1000
        self.__last_step_time: float = 0

    def step(self, now: float) -> bool:
        pass

    def is_complete(self) -> bool:
        pass

    def finalize(self) -> bool:
        pass

    def _time_for_update(self, now: float) -> bool:
        if (now - self.__last_step_time) > self.update_interval_sec:
            self.__last_step_time = now
            return True
        return False
