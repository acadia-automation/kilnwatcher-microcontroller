# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class Color:
    """
    This class is used to represent and work with Color values in a slightly
    more friendly manner than the NeoPixel default, which is a tuple(int, int,
    int, int) format
    """
    def __init__(self, red: int, green: int, blue: int, white: int):
        self.value = (red, green, blue, white)

    @staticmethod
    def rgbw_color_tuple_from_rgb(color_value: int) -> 'Color':
        """
        Converts a packed, rgb, integer color value to a Color object.

        :param color_value:
        int
            the numeric color value to be converted
        :return:
        Color
            the converted color as an object
        """
        blue = color_value & 0xFF
        green = (color_value >> 8) & 0xFF
        red = (color_value >> 16) & 0xFF
        return Color(red, green, blue, 0)

    @property
    def red(self):
        return self.value[0]

    @property
    def green(self):
        return self.value[1]

    @property
    def blue(self):
        return self.value[2]

    @property
    def white(self):
        return self.value[3]
