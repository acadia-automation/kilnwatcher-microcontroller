#!/bin/bash

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

# Source locations
# REVIEW BEFORE INSTALL: Set this appropriately
TEMP_DIR="/tmp"
CURL="/usr/bin/curl"
TAR="/usr/bin/tar"
MOUNT="/usr/bin/mount"
RSYNC="/usr/bin/rsync"

# These shouldn't change unless the source repository does
INSTALL_ARCHIVE_BASE_URL="http://40newthings.com/kilnwatcher"
CURL_OPTS="-fsSL --output"
INSTALL_ARCHIVE_MICROCONTROLLER="kilnwatcher-microcontroller.tar.gz"
KILNWATCHER_MICROCONTROLLER_ROOT="kilnwatcher-microcontroller"
RSYNC_EXCLUDES_FILE="install/rsync_excludes"

# Target locations
# REVIEW BEFORE INSTALL: Set these appropriately
INSTALL_LOC="/mnt/CIRCUITPY"

# precompile some useful variable
starting_dir=$PWD
datestr=`date +"%Y%m%d.%H.%M.%S"`

install_source_path="${TEMP_DIR}/${KILNWATCHER_MICROCONTROLLER_ROOT}"
kilnwatcher_microcontroller_path="${INSTALL_LOC}"
kilnwatcher_microcontroller_libs_path="${kilnwatcher_microcontroller_path}/lib"
rsync_excludes_path="${install_source_path}/${RSYNC_EXCLUDES_FILE}"

download_command="${CURL} ${CURL_OPTS} ${TEMP_DIR}/${INSTALL_ARCHIVE_MICROCONTROLLER} ${INSTALL_ARCHIVE_BASE_URL}/${INSTALL_ARCHIVE_MICROCONTROLLER}"
rsync_command="${RSYNC} -r --delete --exclude-from=${rsync_excludes_path} ${kilnwatcher_source_path} ${kilnwatcher_microcontroller_path}"


echo ""
echo "Fetching Kilnwatcher Microcontroller install files..."

# download the installation archive to tmp
${download_command}

# unpack the install archive
if [[ -d "${install_source_path}" ]]; then
  echo "    -- Previous Kilnwatcher Microcontroller installation left install files. Removing..."
  rm -Rf ${install_source_path}
fi

mkdir ${install_source_path}
echo "    -- Created install directory: ${install_source_path}."

cd ${install_source_path}
${TAR} -zxf ${TEMP_DIR}/${INSTALL_ARCHIVE_MICROCONTROLLER}
echo "    -- Extracted sources."


echo ""
echo "Installing Kilnwatcher Microcontroller files..."

# Verify that the microcontroller is mounted as a drive
if [[ ! -d "${kilnwatcher_microcontroller_path}" ]]; then
  echo "Mount point ${kilnwatcher_microcontroller_path} does not exist. Failing."
  exit -1
elif [[ ! -d "${kilnwatcher_microcontroller_libs_path}" ]]; then
  echo "Microcontroller lib path not available, attempting to mount microcontroller at ${kilnwatcher_microcontroller_path}"
  sudo ${MOUNT} ${kilnwatcher_microcontroller_path}
  if [[ ! -d "${kilnwatcher_microcontroller_libs_path}" ]]; then
    echo "    -- microcontroller not mounted. Failing..."
    exit -2
  fi
  echo "    -- microcontroller mounted. Proceeding..."
fi
echo ""
echo ${rsync_command}
${rsync_command} || exit -3
echo ""
echo "    -- Installed Kilnwatcher Microcontroller to ${kilnwatcher_microcontroller_path}"
echo ""


# Clean up installation files
echo "Removing microcontroller installation files..."
echo ""
rm -Rf ${install_source_path}
rm ${TEMP_DIR}/${INSTALL_ARCHIVE_MICROCONTROLLER}


echo "Microcontroller installation complete."
echo ""
