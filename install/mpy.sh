# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

rm -Rf /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy
mkdir /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy

find . -name \*.py -exec /Users/rangerer/scripts/mpy-cross-macos-catalina-7.1.1 {} \;
find . -name \*.mpy -print | cpio -updm /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy
find . -name \*.mpy -exec rm -v {} \;

# Clean up the fact that CircuitPython doesn't like main.py to have .mpy
cp -v ./main.py /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy
rm -v /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy/main.mpy

# Remove tests
rm -Rfv /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy/test

# Move files to CIRCUITPY
cp -R /Users/rangerer/tmp/kilnwatcher-microcontroller-mpy/* /Volumes/CIRCUITPY
