# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class TemperatureSensor:
    """
    An abstract class that should be the super of all temperature sensors. It
    supports a primary and secondary sensor and conversion utility.
    """

    def read_primary_sensor(self) -> int:
        """
        Reads a temperature from the primary sensor on the device.

        :return:
        int
            temperature in degrees Fahrenheit
        """
        return int(TemperatureSensor.convert_c_to_f(self._read_primary_sensor_raw()))

    def _read_primary_sensor_raw(self) -> float:
        """
        Reads a temperature from the primary sensor on the device in Celsius

        :return:
        float
            temperature in degrees Celsius
        """
        raise NotImplementedError()

    def read_secondary_sensor(self) -> int:
        """
        Reads a temperature from the primary sensor on the device.

        :return:
        int
            temperature in degrees Fahrenheit
        """

    def _read_secondary_sensor_raw(self) -> float:
        """
        Reads a temperature from the primary sensor on the device in Celsius.

        :return:
        float
            temperature in degrees Celsius
        """
        raise NotImplementedError()

    @staticmethod
    def convert_c_to_f(temp_c: float) -> float:
        """
        Converts a Celsius temperature to Fahrenheit

        :param temp_c:
        int
            Temperature in Celsius
        :return:
        int
            Temperature in Fahrenheit
        """
        return (temp_c * 1.8) + 32
