# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import busio
from adafruit_mlx90614 import MLX90614

from sensors.temperature_sensor import TemperatureSensor


class CaseTemperatureSensor(TemperatureSensor):

    def __init__(self, i2c: busio.I2C):
        self.__sensor = MLX90614(i2c)

    def _read_primary_sensor_raw(self) -> float:
        return self.__sensor.object_temperature

    def _read_secondary_sensor_raw(self) -> float:
        return self.__sensor.ambient_temperature
