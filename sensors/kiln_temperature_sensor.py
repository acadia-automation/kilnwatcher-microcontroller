# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import busio
from adafruit_mcp9600 import MCP9600

from sensors.temperature_sensor import TemperatureSensor


class KilnTemperatureSensor(TemperatureSensor):

    def __init__(self, i2c: busio.I2C):
        self.__sensor = MCP9600(i2c, tcfilter=2)

    def _read_primary_sensor_raw(self) -> float:
        return self.__sensor.temperature

    def _read_secondary_sensor_raw(self) -> float:
        return self.__sensor.ambient_temperature
