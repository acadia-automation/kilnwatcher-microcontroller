# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import busio
from adafruit_sht31d import SHT31D

from sensors.temperature_sensor import TemperatureSensor


class AmbientTemperatureSensor(TemperatureSensor):

    def __init__(self, i2c: busio.I2C):
        self.__sensor = SHT31D(i2c)

    def _read_primary_sensor_raw(self) -> float:
        return self.__sensor.temperature

    def _read_secondary_sensor_raw(self) -> float:
        return self.__sensor.relative_humidity
