# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import rainbowio

from actions.action import Action
from indicators.indicator import Indicator
from indicators.color import Color
from indicators.indicator_alchemist import IndicatorAlchemist
from indicators.transmutation_fade import TransmutationFade


class ActionIndicatorKilnTempRange(Action):

    def __init__(self, indicator: Indicator, alchemist: IndicatorAlchemist):
        self.__indicator: Indicator = indicator
        self.__alchemist: IndicatorAlchemist = alchemist
        self.__action_response = "No Action"

    def perform_action(self, data=None):
        # by all rights, this should check for isnumeric(), but I can't get it
        # to acknowledge the decoded bytes as a str and let me call the
        # function
        #    and data.isnumeric()
        if data:
            if self.__alchemist.transmute_immediately(
                    TransmutationFade(self.__indicator, 15, self.__indicator.current_color(),
                                      self.__choose_color_for_temp(int(data)), 10)):
                self.__action_response = Action.SUCCESS
        else:
            self.__action_response = f"Invalid Temperature Data for kiln temp: {data}"

    def get_action_response(self) -> str:
        return self.__action_response

    @staticmethod
    def __choose_color_for_temp(temperature: int) -> Color:
        if temperature <= 200:
            # If the temperature is below 200, go Green
            return Color(40, 240, 0, 0)
        elif temperature > 2200:
            return Color(255, 0, 0, 0)
        else:
            # If the temperature is above 200, choose a point on the wheel between
            # yellow (cooler:~40 on the wheel) and bright red (hotter: 0 on the wheel)
            # 200:2200::40:0
            color_point = (((temperature - 200) / 2000) * -40) + 40
            return Color.rgbw_color_tuple_from_rgb(rainbowio.colorwheel(int(color_point)))
