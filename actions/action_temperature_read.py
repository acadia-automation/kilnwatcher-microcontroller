# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from actions.action import Action
from sensors.temperature_sensor import TemperatureSensor


class ActionTemperatureRead(Action):

    def __init__(self, temp_sensor: TemperatureSensor):
        self.__temp_sensor = temp_sensor
        self.__result = 9999

    def perform_action(self, data=None):
        self.__result = self._read_sensor()

    def get_action_response(self) -> str:
        # All temps get converted into string representations of int
        return str(self.__result)

    def _read_sensor(self) -> int:
        raise NotImplementedError
