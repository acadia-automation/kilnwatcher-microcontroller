# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from actions.action import Action
from indicators.indicator import Indicator
from indicators.indicator_alchemist import IndicatorAlchemist
from indicators.transmutation_fade import TransmutationFade
from indicators.transmutation_rainbow import TransmutationRainbow


class ActionIndicatorTest(Action):

    def __init__(self, indicator: Indicator, alchemist: IndicatorAlchemist):
        self.__indicator: Indicator = indicator
        self.__alchemist: IndicatorAlchemist = alchemist
        self.__action_response = "No Action"

    def perform_action(self, data: str = None):
        if self.__alchemist.transmute_immediately(
                TransmutationRainbow(self.__indicator, 15, 5)) and self.__alchemist.queue_transmutation(
                TransmutationFade(self.__indicator, 15, Indicator.COLOR_OFF, self.__indicator.target_color(), 30,
                                  mode=TransmutationFade.MODE_BOUNCE)):
            self.__action_response = Action.SUCCESS

    def get_action_response(self) -> str:
        return self.__action_response
