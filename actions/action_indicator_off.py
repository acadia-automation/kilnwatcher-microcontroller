# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from actions.action import Action
from indicators.indicator import Indicator
from indicators.indicator_alchemist import IndicatorAlchemist
from indicators.transmutation_fade import TransmutationFade


class ActionIndicatorOff(Action):

    def __init__(self, indicator: Indicator, alchemist: IndicatorAlchemist):
        self.__indicator: Indicator = indicator
        self.__alchemist: IndicatorAlchemist = alchemist
        self.__action_response = "No Action"

    def perform_action(self, data: str = None):
        if self.__alchemist.transmute_immediately(
                TransmutationFade(self.__indicator, 15, self.__indicator.current_color(), Indicator.COLOR_OFF, 30)):
            self.__action_response = Action.SUCCESS

    def get_action_response(self) -> str:
        return self.__action_response
