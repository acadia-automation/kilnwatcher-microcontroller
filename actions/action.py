# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

class Action:
    SUCCESS = 'Success\n'

    INDICATOR_ON_KILN = 'iOK'
    INDICATOR_OFF_KILN = 'iFK'
    INDICATOR_RANGE_KILN = 'iRK'
    INDICATOR_WARN_KILN = 'iWK'
    INDICATOR_TEST_KILN = 'iTK'

    INDICATOR_ON_CASE = 'iOC'
    INDICATOR_OFF_CASE = 'iFC'
    INDICATOR_WARN_CASE = 'iWC'
    INDICATOR_TEST_CASE = 'iTC'

    INDICATOR_ON_FAN = 'iOF'
    INDICATOR_OFF_FAN = 'iFF'
    INDICATOR_WARN_FAN = 'iWF'
    INDICATOR_TEST_FAN = 'iTF'

    AMBIENT_READ_HUMIDITY = 'tH'
    AMBIENT_READ_TEMP = 'tT'
    CASE_READ_TEMP = 'tC'
    CASE_READ_HOUSING_TEMP = 'tS'
    KILN_READ_TEMP = 'tK'
    KILN_READ_AMP_TEMP = 'tA'

    def perform_action(self, data=None):
        raise NotImplementedError()

    def get_action_response(self) -> str:
        raise NotImplementedError()
