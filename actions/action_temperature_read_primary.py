# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

from actions.action_temperature_read import ActionTemperatureRead


class ActionTemperatureReadPrimary(ActionTemperatureRead):

    def _read_sensor(self) -> int:
        return self.__temp_sensor.read_primary_sensor()
