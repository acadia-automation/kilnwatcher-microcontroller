#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import logging
import unittest

from indicators.indicator_alchemist import IndicatorAlchemist
from indicators.indicator_fake import IndicatorFake
from indicators.transmutation_fake import TransmutationFake

logger = logging.getLogger(__name__)


class TestIndicatorAlchemist(unittest.TestCase):

    def test_add_one_tmu_to_queue_success(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu = TransmutationFake(ind, 10)

        self.assertEqual(ia.queue_transmutation(tmu), 1, "Queue length returned after adding tmu is incorrect")
        self.assertIn(ind, ia.transmutation_queues, "Expected transmutation to be in the queue")
        self.assertIn(tmu, ia.transmutation_queues[ind], "Transmutation not in the queue")

    def test_add_multiple_tmu_to_queue_success(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu = (TransmutationFake(ind, 10), TransmutationFake(ind, 20), TransmutationFake(ind, 30))

        self.assertEqual(ia.queue_transmutation(tmu[0]), 1, "Queue length returned after adding tmu is incorrect")
        self.assertEqual(ia.queue_transmutation(tmu[1]), 2, "Queue length returned after adding tmu is incorrect")
        self.assertIn(ind, ia.transmutation_queues, "Expected transmutation to be in the queue")
        self.assertIn(tmu[0], ia.transmutation_queues[ind], "Transmutation not in the queue")
        self.assertIn(tmu[1], ia.transmutation_queues[ind], "Transmutation not in the queue")

        self.assertEqual(ia.queue_transmutation(tmu[2]), 3, "Queue length returned after adding tmu is incorrect")
        self.assertIn(tmu[2], ia.transmutation_queues[ind], "Transmutation not in the queue")

    def test_add_multiple_tmu__multiple_ind_to_queue_success(self):
        ia = IndicatorAlchemist()
        ind1 = IndicatorFake(1)
        ind2 = IndicatorFake(2)
        tmu1 = TransmutationFake(ind1, 10)
        tmu2 = TransmutationFake(ind1, 20)
        tmu3 = TransmutationFake(ind2, 30)

        self.assertEqual(ia.queue_transmutation(tmu1), 1, "Queue length returned after adding tmu is incorrect")
        self.assertEqual(ia.queue_transmutation(tmu3), 1, "Queue length returned after adding tmu is incorrect")
        self.assertIn(ind1, ia.transmutation_queues, "Expected transmutation to be in the queue")
        self.assertIn(ind2, ia.transmutation_queues, "Expected transmutation to be in the queue")
        self.assertIn(tmu1, ia.transmutation_queues[ind1], "Transmutation not in the queue")
        self.assertIn(tmu3, ia.transmutation_queues[ind2], "Transmutation not in the queue")

        self.assertEqual(ia.queue_transmutation(tmu2), 2, "Queue length returned after adding tmu is incorrect")
        self.assertIn(tmu2, ia.transmutation_queues[ind1], "Transmutation not in the queue")

    def test_transmute_immediately_success(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu = TransmutationFake(ind, 10)

        self.assertTrue(ia.transmute_immediately(tmu), "Problem adding immediate transmutation")
        self.assertIn(ind, ia.current_transmutations, "Expected transmutation to be in the queue")
        self.assertEqual(tmu, ia.current_transmutations[ind], "Transmutation not in the queue")

    def test_transmute_immediately_then_enqueue_success(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu1 = TransmutationFake(ind, 10)
        tmu2 = TransmutationFake(ind, 20)

        self.assertTrue(ia.transmute_immediately(tmu1), "Problem adding immediate transmutation")
        self.assertIn(ind, ia.current_transmutations, "Expected transmutation to be in the queue")
        self.assertEqual(tmu1, ia.current_transmutations[ind], "Transmutation not in the queue")

        self.assertEqual(ia.queue_transmutation(tmu2), 1, "Queue length returned after adding tmu is incorrect")
        self.assertIn(ind, ia.transmutation_queues, "Expected transmutation to be in the queue")
        self.assertIn(tmu2, ia.transmutation_queues[ind], "Transmutation not in the queue")

    def test_transmute_immediately_then_step_not_complete_tmu_step_success(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu = TransmutationFake(ind, 10, is_complete_val=False, step_pass=True, finalize_pass=True,
                                time_for_update_val=True)

        self.assertTrue(ia.transmute_immediately(tmu), "Problem adding immediate transmutation")
        self.assertTrue(ia.step(), "Step should have returned successfully.")

    def test_transmute_immediately_then_step_not_complete_tmu_step_fail(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu = TransmutationFake(ind, 10, is_complete_val=False, step_pass=False, finalize_pass=True,
                                time_for_update_val=True)

        self.assertTrue(ia.transmute_immediately(tmu), "Problem adding immediate transmutation")
        self.assertFalse(ia.step(), "Step should have returned failure.")

    def test_transmute_immediately_then_step_tmu_complete_empty_queue(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu = TransmutationFake(ind, 10, is_complete_val=True, step_pass=True, finalize_pass=True,
                                time_for_update_val=True)

        self.assertTrue(ia.transmute_immediately(tmu), "Problem adding immediate transmutation")
        self.assertTrue(ia.step(), "Step should have returned success.")
        self.assertNotIn(ind, ia.current_transmutations, "Indicator key should not be in current TMUs.")

    def test_transmute_immediately_then_step_tmu_complete_run_queue(self):
        ia = IndicatorAlchemist()
        ind = IndicatorFake(1)
        tmu1 = TransmutationFake(ind, 10, is_complete_val=True, step_pass=True, finalize_pass=True,
                                time_for_update_val=True)
        tmu2 = TransmutationFake(ind, 20, is_complete_val=True, step_pass=True, finalize_pass=True,
                                time_for_update_val=True)

        self.assertTrue(ia.transmute_immediately(tmu1), "Problem adding immediate transmutation")
        self.assertEqual(ia.queue_transmutation(tmu2), 1, "Problem adding transmutation to the queue")
        self.assertIn(ind, ia.current_transmutations, "Indicator key should be in current TMUs.")
        self.assertEqual(tmu1, ia.current_transmutations[ind], "Transmutation 1 should be in current TMUs.")
        self.assertTrue(ia.step(), "Step should have returned success.")
        self.assertIn(ind, ia.current_transmutations, "Indicator should still be in current TMUs.")
        self.assertEqual(tmu2, ia.current_transmutations[ind], "Transmutation 2 should be in current TMUs.")


if __name__ == '__main__':
    unittest.main()
