#!/usr/bin/env python3

# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import unittest
import logging

from control.action_registration import ActionRegistration
from control.request_registry import RequestRegistry

logger = logging.getLogger(__name__)


class TestActionRegistration(unittest.TestCase):

    def test_registration_success(self):
        registry = ActionRegistration.register_available_actions("test", "test", "test", "test", "test", "test")
        self.assertIsInstance(registry, RequestRegistry, "Should have returned a RequestRegistry object")


if __name__ == '__main__':
    unittest.main()
