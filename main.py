# SPDX-FileCopyrightText: Copyright 2022 Ronnie Angerer
# SPDX-License-Identifier: GPL-3.0-or-later

import board
import busio
import neopixel
import usb_cdc

from control.action_registration import ActionRegistration
from control.request_processor import RequestProcessor
from indicators.indicator import Indicator
from indicators.indicator_alchemist import IndicatorAlchemist
from indicators.indicator_impl import IndicatorImpl
from sensors.ambient_temperature_sensor import AmbientTemperatureSensor
from sensors.case_temperature_sensor import CaseTemperatureSensor
from sensors.kiln_temperature_sensor import KilnTemperatureSensor


# get a reference to the serial channel from the core circuitpython services
serial = usb_cdc.console

# Initialize neopixel indicators
neopixels = neopixel.NeoPixel(board.D13, 21, auto_write=False, pixel_order=neopixel.GRBW)
kiln_indicator = IndicatorImpl(1, neopixels, Indicator.COLOR_KILN_HOT_GREEN)
case_indicator = IndicatorImpl(2, neopixels, Indicator.COLOR_CASE_HOT_RED)
fan_indicator = IndicatorImpl(0, neopixels, Indicator.COLOR_FAN_ON_BLUE)

# Initialize the sensors
i2c = busio.I2C(board.D11, board.D10, frequency=100000)
kiln_temp_sensor = KilnTemperatureSensor(i2c)
case_temp_sensor = CaseTemperatureSensor(i2c)
ambient_temp_sensor = AmbientTemperatureSensor(i2c)

# Initialize the request processor
request_processor = RequestProcessor(serial,
                                     ActionRegistration.register_available_actions(kiln_indicator, case_indicator,
                                                                                   fan_indicator, ambient_temp_sensor,
                                                                                   case_temp_sensor, kiln_temp_sensor))

# Initialize the TransmutationAlchemist
alchemist = IndicatorAlchemist.get_alchemist()

while True:
    # Deal with potential requests
    request_processor.process_requests()

    # process indicator behaviors
    alchemist.step()
